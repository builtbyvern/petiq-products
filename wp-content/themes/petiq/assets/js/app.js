jQuery(function($){

   $(document).foundation();

    smoothScroll.init({
        easeing: 'easeOutCubic',
        offset: 160
    });

    //slick dat nav
    $('#menu').slicknav({
        label: '',
        prependTo: '#mobilenav'
    });




    // animate the sandiwch
    $('.sandwich a').click(function(){
        $('#sammich').toggleClass('fa-bars').toggleClass('fa-close');
    });


    $(window).load(function(){

        var biscuitNum = $( ".biscuit" ).length;

        for ( var i = 1; i <= biscuitNum; i++ ) {
            var bDelay = Math.random() * 2, // set this beteween .5 second and 2
                bRotation = Math.random() * (900 - -500) + -500; // rotation left or right from 900 to - 500

            TweenLite.to( $( '.b-' + i ), 4, {
                delay: bDelay,
                ease: Power2.easeIn,
                rotation: bRotation,
                y:800
            });
        }


    });


// coupon bar


TweenLite.to($('#coupon'),1, {
    marginTop: 0,
    ease:Cubic.easeInOut,
    delay: 3
});


// open that lovely modal



     // check all those boxes
     $('.all-checker').click(function(e){
        e.preventDefault();
        $('.where-to').each(function(){
            $(this).prop('checked', true);
        });
     });

 // great cookie stuff

 // if ( Cookies.get('newsletterPopup') != true) {
 //     $('#subscribehere').foundation('open');
 // } else {
//
 // }


    $('.close-button').click(function(){
        Cookies.set('newsletter', 'seen');
    });

    if ( Cookies.get('newsletter') !== 'seen')
    {
        $('#subscribehere').foundation('open');
    }

    $(document).ready(function(){
        $('.the-slider').slick({
            arrows: false,
            autoplay: true,
            autoplaySpeed: 4000,
            cssEase: 'cubic-bezier(0.165, 0.840, 0.440, 1.000)',
            pauseOnHover: false,
            speed:600,
            useTransform: true
            // centerMode: true
        });
    });
});
