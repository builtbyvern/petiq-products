
<?php 
if ( has_post_thumbnail() ) {
    $imageurl = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
} 
?>

<div class="hero" style="background-image: url(<?php echo $imageurl[0]; ?>)">
  <div class="row">
    <div class="small-12 medium-6 columns">
      <?php the_content(); ?>
    </div>
  </div>
</div>

<section class="superfoods treat-block">

  <div class="row align-center">
    <div class="small-12 columns">
  		<h3 class='section-title'>Select a PetIQ™ Premium Treat</h3>
  	</div>
  </div>  

  <div class="row align-center">

  	<?php 
  		$args = array( 'post_type' => 'products', 'posts_per_page' => 10, 'category_name' => 'superfood'  );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); 

			$image = get_field('thumbnail_image');
		?>
			<div class="small-6 medium-3 columns">
				<div class="food-bucket">
					<a href="<?php the_permalink(); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php the_title(); ?>"></a>
					<h4><?php the_title(); ?></h4>
					<a class="button white" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
				</div>
			</div>
			
  	<?php endwhile; ?>
  	<?php wp_reset_query(); ?>

  </div>
</section>


<section class="treat-block">

  <div class="row align-center">

   	<?php 
  		$args = array( 'post_type' => 'products', 'posts_per_page' => 10, 'category_name' => 'treats'  );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); 

		$image = get_field('thumbnail_image');
	?>
		<div class="small-6 medium-3 columns">
			<div class="food-bucket">
				<a href="<?php the_permalink(); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php the_title(); ?>"></a>
				<h4><?php the_title(); ?></h4>
				<a class="button white" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
			</div>
		</div>
			
  	<?php endwhile; ?>
  	<?php wp_reset_query(); ?>

  </div>
</section>


	
<?php /* $sfimage = get_field('superfood_benefits_image'); ?>


<section class="superfood-benefits">
		<div class="row">
			<div class="small-12 medium-6 columns align-self-middle">
				<img src="<?php echo $sfimage['url']; ?>" alt="">
			</div>

			<div class="small-12 medium-6 columns align-self-middle">
			<div class="text">
				<?php the_field('superfood_benefits_text'); ?>
				<?php if( have_rows('button') ): ?>
					<?php while ( have_rows('button') ): the_row(); 
						$link = get_sub_field('link');
						$text = get_sub_field('text');
					?>
						<a class="button" href="<?php echo $link ?>"><?php echo $text ?></a>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
  </div>
</section>

<?php */ ?>


