<article <?php post_class(); ?>>


	<section class="hero">

		<div class="the-slider">
			<div><a href="/products"><img src="<?php echo get_template_directory_uri() ?>/assets/img/slide1.jpg" alt=""></a></div>
			<div id="darkblue"><a href="/products"><img src="<?php echo get_template_directory_uri() ?>/assets/img/slide2.jpg" alt=""></a></div>
		</div>

		 <?php /*
	    <div class="row align-center">
	      <div class="column small-4 show-for-medium">
	        <div id="treatwrap">
	          <div id="treats">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/img/blue-biscuit.png" alt="" class="biscuit b-1 one">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/img/blue-biscuit.png" alt="" class="biscuit b-2 two">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/img/blue-biscuit.png" alt="" class="biscuit b-3 three">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/img/blue-biscuit.png" alt="" class="biscuit b-4 four">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/img/blue-biscuit.png" alt="" class="biscuit b-5 five">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/img/blue-biscuit.png" alt="" class="biscuit b-6 six">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/img/blue-biscuit.png" alt="" class="biscuit b-7 seven">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/img/blue-biscuit.png" alt="" class="biscuit b-8 eight">
	          </div>
	        </div>
	      </div>
	    </div>

	    <div class="row align-middle">
	      <div class="column small-12">
	        <img src="<?php echo get_template_directory_uri() ?>/assets/img/mobile-hero.jpg" class="show-for-small-only" alt="">
	        <img class="show-for-medium" src="<?php echo get_template_directory_uri() ?>/assets/img/hero-bag.jpg" alt="">
	      </div>
	    </div>

			 */ ?>

	  </section>


<!--
	<section class="hero-slider">
		<div class="the-slider">
			<div>your content</div>
			<div>your content</div>
			<div>your content</div>
		</div>
	</section>
-->
<?php /* ?>
	<section class="five-stars">
		<div class="row align-right">

			<div class="column small-12 medium-6">

				<?php if( have_rows('bag_slider') ): ?>
					<div class="the-slider">
					<?php while( have_rows('bag_slider') ): the_row();
						$image = get_sub_field('image');
						$link = get_sub_field('link');
					?>
						<div><a href="<?php echo $link; ?>"><img src="<?php echo $image['url']; ?>" alt=""></a></div>
					<?php endwhile; ?>
					</div>
				<?php endif; ?>






				<div class="rating">
					<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> (389 REVIEWS)
				</div>
			</div>

			<div class="column small-12 medium-3">
				<h3><strong>PETIQ&trade;</strong> Premium Treats deliver the ultimate tasting and nutritional snack your dog will love. </h3>
				<a href="#" class="button">VIEW PRODUCTS</a>
			</div>

		</div>
	</section>
<?php */ ?>

	  <section class="premium-ingredients">
	    <div class="row align-middle">
	      <div class="column small-12 medium-6">
	        <div class="bones">
	          <img src="<?php echo get_template_directory_uri() ?>/assets/img/blue-biscuit.png" alt="" class="show-for-medium bone bone-1">
	          <img src="<?php echo get_template_directory_uri() ?>/assets/img/blue-biscuit.png" alt="" class="show-for-medium bone bone-2">
	        </div>
	        <div class="content">
	        	<?php the_field('wholesome_ingredients'); ?>
	          <a href="/products" class="button">VIEW PRODUCTS</a>
	        </div>

	      </div>
	      <div class="column small-12 medium-6">
	      	<img src="<?php echo get_template_directory_uri() ?>/assets/img/smiling-dogs.jpg" alt="">
	      </div>
	    </div>
	  </section>

		<?php /* ?>
	  <section class="super">
	    <div class="row ">
	      <div class="column small-12 medium-3 align-self-middle">

	      	<?php if( have_rows('what_makes_it_super') ): ?>
	      		<?php while( have_rows('what_makes_it_super') ): the_row();
							$position = get_sub_field('position');
							$content = get_sub_field('content');
						?>
							<?php if ($position === 'left'): ?>
								<div class="text">
									<?php echo $content ?>
								</div>
							<?php endif; ?>

						<?php endwhile; ?>
					<?php endif; ?>

	      </div>
	      <div class="column small-12 medium-6 bag align-self-middle">
	        <h3>What Makes <strong>PetIQ&trade;</strong> Super</h3>
	        <img src="<?php echo get_template_directory_uri() ?>/assets/img/natural-superfood-biscuits.png" alt="">
	        <a href="/products" class="button white">VIEW PRODUCTS</a>
	      </div>
	      <div class="column small-12 medium-3 align-self-middle">
	        <?php if( have_rows('what_makes_it_super') ): ?>
	      		<?php while( have_rows('what_makes_it_super') ): the_row();
							$position = get_sub_field('position');
							$content = get_sub_field('content');
						?>
							<?php if ($position === 'right'): ?>
								<div class="text">
									<?php echo $content ?>
								</div>
							<?php endif; ?>

						<?php endwhile; ?>
					<?php endif; ?>
	      </div>
	    </div>
	  </section>

	  <section class="superdogs">
	    <div class="row">
	      <div class="column medium-5">
	       	<?php the_field('superfood_for_superdogs'); ?>
	        <a href="#" class="button">LEARN MORE</a>
	      </div>
	    </div>
	  </section>

	  <section class="two-products">
	    <div class="expanded row">

	    <?php if( have_rows('treat_boxes') ): ?>
	    	<?php $row_count = 0; ?>
	    	<?php while( have_rows('treat_boxes') ): the_row();
	    		$image = get_sub_field('image');
	    		$link = get_sub_field('link');
	    		$title = get_sub_field('title');
	    		$description = get_sub_field('description');


	    	?>
	    		<div class="column small-12 medium-6 <?php if ($row_count == 0): echo 'darkblue'; endif;?>">
		        <div class="content">
							<?php if( !empty($image) ): ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							<?php endif; ?>

							<h3><?php echo $title; ?></h3>
					    <p><?php echo $description; ?></p>
		          <a href="<?php echo $link; ?>" class="button white">VIEW PRODUCTS</a>
		        </div>

		      </div>
		      <?php $row_count += 1; ?>
	    	<?php endwhile; ?>
	    <?php endif; ?>


	    </div>
	  </section>

	  <section class="home-reviews">
	    <div class="row align-center">
	      <div class="column small-12">
	        <h3>What&rsquo;s Your <strong>PetIQ&trade;</strong> Experience?</h3>
	      </div>

	      	{% for card in 0..2 %}
				<div class="column small-12 medium-3">
		          {% include '_includes/reviewCard' %}
		        </div>
	      	{% endfor %}



	    </div>
	  </section>
	*/ ?>

	<section class="superfoods treat-block">

  <div class="row align-center">
  	<div class="small-12 columns">
  		<h3 class='section-title'>Select a PetIQ™ Premium Treat</h3>
  	</div>

  </div>

  <div class="row align-center">

  	<?php
  		$args = array( 'post_type' => 'products', 'posts_per_page' => 10, 'category_name' => 'superfood'  );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post();

			$image = get_field('thumbnail_image');
		?>
			<div class="small-6 medium-3 columns">
				<div class="food-bucket">
					<a href="<?php the_permalink(); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php the_title(); ?>"></a>
					<h4><?php the_title(); ?></h4>
					<a class="button white" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
				</div>
			</div>

  	<?php endwhile; ?>
  	<?php wp_reset_query(); ?>

  </div>
</section>


<section class="treat-block">

  <div class="row align-center">

   	<?php
  		$args = array( 'post_type' => 'products', 'posts_per_page' => 10, 'category_name' => 'treats'  );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post();

			$image = get_field('thumbnail_image');
		?>
			<div class="small-6 medium-3 columns">
				<div class="food-bucket">
					<a href="<?php the_permalink(); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php the_title(); ?>"></a>
					<h4><?php the_title(); ?></h4>
					<a class="button white" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
				</div>
			</div>

  	<?php endwhile; ?>
  	<?php wp_reset_query(); ?>

  </div>
</section>

	  <section class="pre-footer">
	    <div class="expanded row">
	      <div class="contact small-12 medium-6 column">
	        <div class="mask"></div>
	        <div class="content">
	          <h4>Sign up for the <strong>PetIQ™</strong> newsletter and get tons of information and deals right in your inbox. </h4>
	          <form>
	            <div class="row align-center">
	              <div class="medium-7 columns">
	                <label for="name" >Name </label>
	                <input type="text" placeholder="Name">
	              </div>
	              <div class="medium-7 columns">
	                <label>Input Label</label>
	                <input type="text" placeholder="Email Address">
	              </div>
	            </div>
	            <div class="row align-center">
	              <div class="medium-5 columns">
	                <input type="submit" value="SIGN UP" class="button expanded">
	              </div>
	            </div>
	          </form>
	        </div>
	      </div>
	      <div class="find small-12 medium-6 column ">
	        <div class="mask"></div>
	        <div class="content">
	          <h4>Where do I find <strong>PetIQ™</strong> Products</h4>
	          <p>PetIQ™ products are available at Walmart, Sam’s Club and other retailers nationwide.  Click a link below to find a store near you.</p>
	          <br>
	          <div class="row align-center small-collapse">
	            <?php  /*
	            <div class="small-5 columns">
	              <h2>Walmart</h2>
	            </div>
	            <div class="small-5 columns">
	              <h2>Sam&rsquo;s Club</h2>
	            </div>
	            */ ?>
	          </div>

	          <a href="/buy" class="button">FIND A LOCATION</a>
	        </div>
	      </div>
	    </div>
	  </section>

  </article>
