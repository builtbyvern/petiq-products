		<section id="contact">
			<div class="row">
				<div class="small-12 medium-6 medium-offset-6 column">
					
					<?php the_content(); ?>


					<p>For business related questions, email <a href="mailto:customerservice@petiq.com">customerservice@petiq.com</a>. Please remember to include your company name.</p>

				</div>
			</div>
		</section>