	<section id="buy" class="hero">
		<div class="row align-center">
			<div class="small-12 medium-8 column">
				<?php the_content(); ?>
			</div>
		</div>
		<div class="row align-center">
			<div class="where-to small-12 medium-5 large-4 column">
				<h1>Walmart</h1>
				<a href="http://www.walmart.com/store/finder" target="_blank" class="button white darkblue">FIND A LOCATION</a>
			</div>
			<div class="where-to small-12 medium-5 large-4 column">
				<h1>Sam&rsquo;s Club</h1>
				<a href="http://www.samsclub.com/sams/shoppingtools/selectaclub.jsp" target="_blank" class="button white darkblue">FIND A LOCATION</a>
			</div>
		</div>
	</section>