<?php /* ?>
<footer class="content-info">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
*/ ?>


<footer id="footer">
  <div class="row">
    <div class="small-12 medium-4 large-5 column">

    	<?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu([
            'theme_location' => 'primary_navigation', 
            'menu_class' => 'footer-nav',
          ]);
        endif;
      ?>
      
    </div>
    <div class="right small-12 medium-8 large-7 column">
      <a href="/buy" class="button">WHERE TO BUY</a>
    </div>
  </div>
  <div class="row">

    <div class="column">
      <h6><?php the_field('petiq_address', 'options') ?></h6>
      <p><?php the_field('disclaimer', 'options') ?></p>
    </div>
  </div>
</footer>


<div class="reveal" id="subscribehere" data-reveal data-close-on-click="true" data-animation-in="scale-in-up" data-animation-out="scale-out-down">
<header>
  <h1>Welcome to </h1>
  <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-white.png" alt="">
</header>
<div class="content">
  <div class="row">
    <div class="small-12 columns">
      <h4>
      STAY IN THE KNOW WITH NEWS, SPECIAL OFFERS AND EXCLUSIVE PERKS FOR INSIDERS.</h5>
      <p>ENTER YOUR EMAIL BELOW TO GET STARTED</p>
    </div>
  </div>
  <!-- Begin MailChimp Signup Form -->
  <div id="mc_embed_signup" class="small-12 columns">
    <form action="//truescience.us12.list-manage.com/subscribe/post?u=d24b5601dba40919691474468&amp;id=bfc3838cb0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
      <div id="mc_embed_signup_scroll">
        <div class="mc-field-group">
          <label for="mce-EMAIL" class="show-for-sr">Email Address  <span class="asterisk">*</span>
          </label>
          <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="EMAIL">
        </div>
        <div class="mc-field-group input-group">
          <input type="checkbox" value="64" name="group[6233][64]" id="mce-group[6233]-6233-0" checked> 
          <label for="mce-group[6233]-6233-0">Yes, I want to get news and offers</label>
        </div>
        <hr>
        <div class="mc-field-group input-group">
          <strong>I'm interested in (<a href="#" class="all-checker">check all that apply</a>)</strong>
          <div class="checkbox"><input class="where-to" type="checkbox" value="1" name="group[6229][1]" id="mce-group[6229]-6229-0"><label for="mce-group[6229]-6229-0">Dog Products</label></div>
          <div class="checkbox"><input class="where-to" type="checkbox" value="2" name="group[6229][2]" id="mce-group[6229]-6229-1"><label for="mce-group[6229]-6229-1">Cat Products</label></div>
          <div class="checkbox"><input class="where-to" type="checkbox" value="4" name="group[6229][4]" id="mce-group[6229]-6229-2"><label for="mce-group[6229]-6229-2">Flea &amp; Tick Treatments</label></div>
          <div class="checkbox"><input class="where-to" type="checkbox" value="8" name="group[6229][8]" id="mce-group[6229]-6229-3"><label for="mce-group[6229]-6229-3">Pet Health &amp; Wellness Products</label></div>
          <div class="checkbox"><input class="where-to" type="checkbox" value="16" name="group[6229][16]" id="mce-group[6229]-6229-4"><label for="mce-group[6229]-6229-4">Promotional Offers &amp; Samples</label></div>
          <div class="checkbox"><input class="where-to" type="checkbox" value="32" name="group[6229][32]" id="mce-group[6229]-6229-5"><label for="mce-group[6229]-6229-5">Joining the Street Team</label></div>
        </div>
        <div id="mce-responses" class="clear">
          <div class="response callout warning" id="mce-error-response" style="display:none"></div>
          <div class="response callout success" id="mce-success-response" style="display:none"></div>
        </div>
        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d24b5601dba40919691474468_bfc3838cb0" tabindex="-1" value=""></div>
        <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
      </div>
    </form>
  </div>

  <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='text';fnames[4]='MMERGE4';ftypes[4]='text';fnames[5]='MMERGE5';ftypes[5]='text';fnames[6]='MMERGE6';ftypes[6]='text';fnames[7]='MMERGE7';ftypes[7]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
  <!--End mc_embed_signup-->
  <button class="close-button" data-close aria-label="Close reveal" type="button">
  <span aria-hidden="true">&times;</span>
  </button>
</div>
