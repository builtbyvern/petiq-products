<?php while (have_posts()) : the_post(); ?>

<?php 
  $heroimagebg = get_field('hero_background_image');
  $heroimage = get_field('hero_image');
  $feedingimage = get_field('feeding_guide_background_image');
?>
       

  <section class="superfood-single-hero hero" style="background-image: url(<?php echo $heroimagebg['url']; ?>)">
    <div class="row align-centered align-middle">
      <div class="small-12 medium-6 columns">
        <img src="<?php echo $heroimage['url']; ?>" alt="">
      </div>
    
      <div class="small-12 medium-6 columns">
        <h1><?php the_title(); ?></h1>
        <?php the_field('hero_description') ?>
        
        <a href="/buy" class="button white darkblue">WHERE TO BUY</a>

      </div>
      
    </div>
  </section>

  <?php if( have_rows('block') ): ?>
    <?php while( have_rows('block') ): the_row(); 
      $title = get_sub_field('title');
      $text = get_sub_field('text');
      $image = get_sub_field('image');
    ?>
      <section id="why-iq" class="hero">
        <div class="row align-center">
          <div class="small-12 medium-6 columns">
            <h3 class="section-title"><?php echo $title; ?></h3>
          </div>
        </div>
        <div class="row align-center">

          <div class="small-12 medium-4 columns">
            <div class="about-iq">
              <?php echo $text ?>
            </div>
          </div>

          <div class="small-12 medium-4 medium-offset-2 columns">
            <img src="<?php echo $image['url'] ?>" alt="">
          </div>
        </div>
      </section> 
    <?php endwhile; ?>
  <?php endif; ?>


  
  <section id="feeding-guide" style="background-image: url(<?php echo $feedingimage['url']; ?>)">
    <div class="row align-center">
      <div class="small-12 medium-6 column">
        <div class="intro">
          <h2>Feeding Guide</h2>
          <?php the_field('feeding_guide_intro'); ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="small-12 medium-4 medium-offset-1 columns">
        <div class="feeding-content">
          <h3>Feeding Instructions</h3>
          <?php the_field('feeding_instructions'); ?>
        </div>
      </div>
      <div class="small-12 medium-4 medium-offset-2 columns">
        <div class="feeding-content">
          <h3>Daily Suggested Maximum Amounts</h3>
          <?php if (have_rows('daily_suggested_maximum_amounts')): ?>
            <dl class="clearfix">
              <?php while(have_rows('daily_suggested_maximum_amounts')) : the_row(); 
                $dog_weight = get_sub_field('dog_weight');
                $percentage = get_sub_field('percentage');
              ?>
                <dt><?php echo $dog_weight; ?></dt>
                <dd>&hellip;&hellip;&hellip;<?php echo $percentage; ?></dd>
              <?php endwhile; ?>   
            </dl>           
          <?php endif ?>
        </div>
      </div>
    </div>

      <div class="row">
      <div class="small-12 medium-4 medium-offset-1 columns">
        <div class="feeding-content">
          <h3>Guranteed Analysis</h3>
          <?php if (have_rows('guaranteed_analysis')): ?>
            <dl class="clearfix">
              <?php while(have_rows('guaranteed_analysis')) : the_row(); 
                $component = get_sub_field('component');
                $percentage = get_sub_field('percentage');
              ?>
                <dt><?php echo $component; ?></dt>
                <dd>&hellip;&hellip;&hellip;<?php echo $percentage; ?></dd>
              <?php endwhile; ?>   
            </dl>           
          <?php endif ?>
        </div>
      </div>
      <div class="small-12 medium-4 medium-offset-2 columns">
        <div class="feeding-content">
          <h3>Ingredients</h3>
          <?php the_field('ingredients'); ?>
        </div>
      </div>
    </div>


  </section>


<?php endwhile; ?>
