
<header id="header">
  <section id="coupon">
    <div class="row">
      <div class="small-12 column">
        <?php the_field('coupon_link', 'option'); ?>
      </div>
    </div>
  </section>

  <div class="row">
    <div class="column">
      <div class="top-bar">

        <div class="top-bar-title">
          <a href="<?= esc_url(home_url('/')); ?>" class="logo">
          
          <?php
            if ( is_front_page() ) {
              echo '<img src="' . get_template_directory_uri() . '/assets/img/logo-white.png" alt="">';
            } else {
              echo '<img src="' . get_template_directory_uri() . '/assets/img/logo-blue.png" alt="">';
            }
          ?>
          </a>
        </div>

        <div class="top-bar-right">
          <?php
            if (has_nav_menu('primary_navigation')) :
              wp_nav_menu([
                'theme_location' => 'primary_navigation',
                'menu_class' => 'menu show-for-medium',
                'menu_id' => 'menu'
              ]);
            endif;
          ?>
        </div>
        <div id="mobilenav"></div>

      </div><!-- / top-bar -->
    </div><!-- /column -->
  </div>
</header>
