
<?php 
if ( has_post_thumbnail() ) {
    $imageurl = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
} 
?>

<section id="faq-hero" class="hero" style="background-image: url(<?php echo $imageurl[0]; ?>)">
	<div class="row align-center">
		<div class="column">
			<h1 class="section-title"><?php the_title(); ?></h1>
		</div>
	</div>
	<div id="faq-titles">

	<?php if( have_rows('faqs') ): 
		$rowcount = 0;
	?>
		<?php while( have_rows('faqs') ): the_row(); 

			// vars
			$q = get_sub_field('question');
			$a = get_sub_field('answer');
			$rowcount ++;

			?>
			
			<?php if ($rowcount % 2 != 0) {	echo "<div class='row align-center'>"; } ?>

				<div class="small-12 medium-6 column">
					<div class="faq-title squeeze">
						<h3><a data-scroll href="#faq<?php echo $rowcount; ?>"><?php echo $q; ?></a></h3>
					</div>
				</div>

			<?php if ($rowcount % 2 == 0) { echo "</div>"; } ?>

		<?php endwhile; ?>
	<?php endif; ?>

	</div>
	
</section>

<section id="faq-answers" class="hero">

	<?php if( have_rows('faqs') ): 
		$rowcount = 0;
	?>
		<?php while( have_rows('faqs') ): the_row(); 
			$q = get_sub_field('question');
			$a = get_sub_field('answer');
			$rowcount ++;
		?>

			<?php if ($rowcount % 2 != 0) {	echo "<div class='row align-center'>"; } ?>
				<div class="small-12 medium-6 column">
					<div id="faq<?php echo $rowcount; ?>" class="faq-answer squeeze">
						<h4 class="faq-question"><?php echo $q ?></h4>
						<?php echo $a ?>
					</div>
				</div>
			<?php if ($rowcount % 2 == 0) { echo "</div>"; } ?>
		<?php endwhile; ?>
	<?php endif; ?>
		
</section>